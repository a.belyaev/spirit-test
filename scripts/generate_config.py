"""
Created by Andrey Belyaev
"""
import argparse
import yaml
from easydict import EasyDict as edict


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--net-type', type=str, default='artifacts_locator')
    parser.add_argument('--out-name', type=str, default='config')
    return parser.parse_args()


def generate_config(net_type):
    artifacts_params = edict(
        shape_range=(50, 1000),
        square_range=(0.01, 0.07),
        artifact_types_count=3
    )
    nets_params = edict(
        target_size=(512, 256),
        batch_size=32,
        context=(0, ),
        data_name='data',
        label_name='label',
        net_type=net_type,
        train_info_path='all_train.json',
        val_info_path='all_test.json',
        test_data_path='',
    )
    conf = edict(nets_params=nets_params, artifacts_params=artifacts_params)
    return conf


def edict2dict(d):
    if isinstance(d, edict):
        return {key: edict2dict(val) for key, val in d.items()}
    else:
        return d


if __name__ == '__main__':
    args = parse_args()
    config = generate_config(args.net_type)
    dict_config = edict2dict(config)

    with open(args.out_name + '.yml', 'w') as f:
        f.writelines(['#####\n',
                      '# Created by Andrey Belyaev\n',
                      '#####\n'])
        yaml.dump(dict_config, f, default_flow_style=False)