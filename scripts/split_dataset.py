"""
Created by Andrey Belyaev
"""
import argparse
import json
import numpy as np
from DataUtils.utils import find_images


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data-path', type=str, required=True)
    parser.add_argument('--parse-recursively', action='store_true', default=False)
    parser.add_argument('--out-path', type=str, required=True)
    parser.add_argument('--train-pct', type=float, default=0.7)
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    paths = find_images(args.data_path, args.parse_recursively)
    idxs = np.random.permutation(len(paths))

    with open(f'{args.out_path}_train.json', 'w') as f:
        json.dump([str(paths[i]) for i in idxs[:int(len(idxs) * args.train_pct)]], f, indent=2)
    with open(f'{args.out_path}_test.json', 'w') as f:
        json.dump([paths[i] for i in idxs[int(len(idxs) * args.train_pct):]], f, indent=2)
