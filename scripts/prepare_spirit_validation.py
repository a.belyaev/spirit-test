"""
Created by Andrey Belyaev
"""
import argparse
import cv2
import json
import os
import os.path as osp
from tqdm import tqdm

from DataUtils.utils import find_images


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data-path', required=True, type=str)
    parser.add_argument('--out-path', required=True, type=str)
    return parser.parse_args()


def prepare_images(img_paths, clean_path, unclean_path):
    clean_img_paths, unclean_img_paths = [], []
    for n, img_path in enumerate(tqdm(img_paths)):
        image = cv2.imread(img_path)
        width = image.shape[1]
        clean_image, unclean_image = image[:, :width // 2], image[:, width // 2:]

        clean_img_path = osp.join(clean_path, f'img_{n:05}.jpg')
        unclean_img_path = osp.join(unclean_path, f'img_{n:05}.jpg')

        cv2.imwrite(clean_img_path, clean_image)
        cv2.imwrite(unclean_img_path, unclean_image)
        clean_img_paths.append(osp.abspath(clean_img_path))
        unclean_img_paths.append(osp.abspath(unclean_img_path))

    return clean_img_paths, unclean_img_paths


if __name__ == '__main__':
    args = parse_arguments()

    source_paths = find_images(args.data_path, True)

    out_clean_path, out_unclean_path = osp.join(args.out_path, 'clean'), osp.join(args.out_path, 'unclean')
    for path in (args.out_path, out_clean_path, out_unclean_path):
        if not osp.exists(path):
            os.mkdir(path)

    clean_paths, unclean_paths = prepare_images(source_paths, out_clean_path, out_unclean_path)

    with open(f'{args.out_path}_info.json', 'w') as f:
        json.dump(list(zip(unclean_paths, clean_paths)), f, indent=2)
