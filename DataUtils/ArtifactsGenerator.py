"""
Created by Andrey Belyaev
"""
import cv2
import numpy as np
from numba import jit


@jit(nopython=True)
def create_gradient_image(m, alpha, beta):
    """ Fast gradient image creating with numba

    :param m: matrix where gradient will be stored
    :type m: np.ndarray
    :param alpha: coefficient
    :type alpha: float
    :param beta: coefficient
    :type beta: float
    :return: matrix with gradient
    """
    cx, cy = m.shape[0] // 2, m.shape[1] // 2
    for i in range(cx * 2):
        for j in range(cy * 2):
            d = np.sqrt((cx - i) ** 2 + (cy - j) ** 2)
            m[i, j] *= np.array([alpha / np.log(d / beta + 1.01) for _ in range(3)])
    return m


class ArtifactsGenerator:
    """Class for artifacts generating

    :param parameters: dictionary with artifacts gen params
    :type parameters: EasyDict
    """
    def __init__(self, parameters):
        self.params = parameters

    def _generate_random_field(self, target_shape):
        """Generate image with target shape with random artifact's fields on it.
        1. Create image with random shape (range from params)
        2. Generate random square to fill with artifacts
        3. Generate random part of ellipse, assign with one of the artifacts types
        4. Do (3) until artifact's square will be greater that generated in (2)
        5. Resize image with artifacts to target shape

        :param target_shape: height and width of target image
        :type target_shape: tuple
        :return: artifacts map
        """
        # Generate image with random size
        image = np.zeros((np.random.randint(*self.params.shape_range), np.random.randint(*self.params.shape_range), 1),
                         dtype=np.uint8)

        # Generate random square to fill with artifacts
        bsr = np.random.random() * (self.params.square_range[1] - self.params.square_range[0]) + self.params.square_range[
            0]

        # Do with ellipse parts until there are enough artifacts
        while len(np.where(image > 0)[0]) / float(image.shape[0] * image.shape[1]) < bsr:
            # Generate random center
            center = (np.random.randint(image.shape[0]), np.random.randint(image.shape[1]))
            # Generate random axes
            axes = (np.random.randint(image.shape[1] // 20, image.shape[1] // 4),
                    np.random.randint(image.shape[0] // 20, image.shape[0] // 4))
            # Generate random angles
            angles = (np.random.randint(0, 360), np.random.randint(0, 360), np.random.randint(0, 360))
            # Generate random artifact type
            color = np.random.randint(self.params.artifact_types_count) + 1
            # Draw ellipse part
            cv2.ellipse(image, center, axes, angles[0], angles[1], angles[2], (color,), -1)

        # Resize image to target shape
        return cv2.resize(image, target_shape, interpolation=cv2.INTER_NEAREST)

    def draw_artifacts(self, image):
        """Generate artifacts map and draw them on image
        There are 4 types of artifacts
        1. Field with random color
        2. Field with changed brightness
        3. Field with random noise
        4. Field with gradient circle, which allows to overflow unit8 max value

        :param image:
        :return:
        """
        artifacts_map = self._generate_random_field(image.shape[:2][::-1])
        # Draw random bright color
        art1_idxs = np.where(artifacts_map == 1)
        image[art1_idxs] = (np.random.randint(0, 250), np.random.randint(0, 250), np.random.randint(0, 250))

        # Change bright
        art2_idxs = np.where(artifacts_map == 2)
        inv_gamma = 1. / ((3 + np.random.random()) ** 2)
        image[art2_idxs] = ((image[art2_idxs].astype(float) / 255.) ** inv_gamma * 255).astype(np.uint8)

        # Create random
        art3_idxs = np.where(artifacts_map == 3)
        image[art3_idxs] = (np.random.random(image[art3_idxs].shape) * 255).astype(np.uint8)

        # Add some gradient circles
        for _ in range(np.random.randint(5)):
            x_start, y_start = np.random.randint(image.shape[0]), np.random.randint(image.shape[1])
            w = min(image.shape[0] - x_start - 1, np.random.randint(image.shape[0] // 5))
            h = min(image.shape[1] - y_start - 1, np.random.randint(image.shape[1] // 5))

            square = image[x_start: x_start + w, y_start: y_start + h]
            img = create_gradient_image(square.astype(float), np.random.random() * 2, (np.random.random() + 1) * 10)

            idxs = np.where(np.random.random(square.shape) < 0.55)
            square[idxs] = img[idxs]
            artifacts_map[x_start: x_start + w, y_start: y_start + h][idxs[:2]] = 4

        return image, artifacts_map
