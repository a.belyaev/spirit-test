"""
Created by Andrey Belyaev
"""
import os
import os.path as osp


def find_images(images_folder, recursive=False, images_formats=('.jpg', '.jpeg', '.png')):
    """Finds images in folder.
    If recursive, also finds all images in all subfolders

    :param images_folder: path to folder
    :param recursive: whether to use recursive searching or not
    :param images_formats: permitted images format, default: jpg, jpeg, png
    :return: array of images paths
    """
    if not recursive:
        good_images_iter = filter(lambda p: p.lower().endswith(images_formats), os.listdir(images_folder))
        paths = list(map(lambda p: osp.join(images_folder, p), good_images_iter))
    else:
        paths = []
        for path in os.listdir(images_folder):
            abs_path = osp.join(images_folder, path)
            if osp.isdir(abs_path):
                paths.extend(find_images(abs_path, recursive, images_formats))
            elif abs_path.lower().endswith(images_formats):
                paths.append(abs_path)

    return paths
