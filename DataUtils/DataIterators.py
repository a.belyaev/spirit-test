"""
Created by Andrey Belyaev
"""
import cv2
import json
import mxnet as mx
import numpy as np

from DataUtils.ArtifactsGenerator import ArtifactsGenerator


class ImagesWithArtifactsAggregator:
    """Aggregates and preprocesses images for network
    Has 3 label types:
    1. 'artifacts_map' - data: image with artifacts, label: artifacts map, task: astifacts localization
    2. 'original_image' - data: image with artifacts, label: original image, task: image restoring
    3. 'from_info' - data, label: images for path's pair in `info_path`, task: validate on Spirit data

    :param info_path: path to json with array of images paths
    :param artifacts_parameters: parameters for artifacts generator
    :param label_type: label type, one of ('artifacts_map', 'original_image', 'from_info')
    :param target_size: size of target images
    """
    def __init__(self, info_path, artifacts_parameters, label_type, target_size=(512, 512)):
        self.label_type = label_type
        assert self.label_type in ('artifacts_map', 'original_image', 'from_info')

        with open(info_path, 'r') as f:
            self.img_paths = json.load(f)
        print(f'Find total {len(self.img_paths)} images')

        self.artifacts_generator = ArtifactsGenerator(artifacts_parameters)
        self.label_type, self.target_size = label_type, target_size

        self.idxs = np.random.permutation(len(self.img_paths))
        self.cur_idx = 0

    def __iter__(self):
        return self

    def reset(self):
        self.idxs = np.random.permutation(len(self.img_paths))
        self.cur_idx = 0

    @staticmethod
    def prepocess_image(img):
        """Preprocesses image for network
        img -> (float)img / 255 -> as_shape(C, W, H)

        :param img: image
        :return: preprocessed image
        """
        if img.max() > 1:
            img = img.astype(float) / 255.
        if len(img.shape) == 2:
            img = img[..., np.newaxis]
        return img.transpose((2, 0, 1))

    @staticmethod
    def preprocess_artifacts_map(artifacts_map):
        """Preprocesses artifacts map

        :param artifacts_map: artifacts map
        :return: preprocessed artifacts map
        """
        am = artifacts_map.copy().astype(float)
        am[np.where(artifacts_map > 0)] = 1.
        return am[np.newaxis, ...]

    def __next__(self):
        """Iterates over dataset with respect to label type

        :return:
        """
        # Check index to overflow
        self.cur_idx += 1
        if self.cur_idx >= len(self.img_paths):
            self.reset()

        try:
            # Generate data for training
            if self.label_type in ('artifacts_map', 'original_image'):
                image_path = self.img_paths[self.idxs[self.cur_idx]]
                image = cv2.imread(image_path)
                image = cv2.resize(image, self.target_size)
                image_with_artifacts, artifacts_map = self.artifacts_generator.draw_artifacts(image.copy())

                data_sample = self.prepocess_image(image_with_artifacts)
                if self.label_type == 'artifacts_map':
                    label_sample = self.preprocess_artifacts_map(artifacts_map)
                else:
                    label_sample = self.prepocess_image(image)

            # Generate data for Spirit validation
            else:
                data_path, label_path = self.img_paths[self.idxs[self.cur_idx]]
                data_sample = self.prepocess_image(cv2.resize(cv2.imread(data_path), self.target_size))
                label_sample = self.prepocess_image(np.mean(cv2.resize(cv2.imread(label_path), self.target_size), axis=-1))

        except:
            data_sample, label_sample = self.__next__()

        finally:
            return data_sample, label_sample


class ImagesWithArtifactsDataIter(object):
    """Iterates over dataset using ImagesWithArtifactsAggregator and create batches for training

    :param info_path: path to json with array of images paths
    :param artifacts_parameters: parameters for artifacts generator
    :param label_type: label type (see Aggregator)
    :param data_names: input nodes names
    :param data_shapes: input nodes shapes
    :param label_names: label nodes names
    :param label_shapes: label nodes shapes
    :param num_batches: num batches per epoch
    """
    def __init__(self, info_path, artifacts_parameters, label_type,
                 data_names, data_shapes, label_names, label_shapes, num_batches=100):
        assert label_type in ('artifacts_map', 'original_image', 'from_info')

        self.target_size = tuple(data_shapes[0][-2:])
        self._provide_data = zip(data_names, data_shapes)
        self._provide_label = zip(label_names, label_shapes)
        self.batch_size, self.num_batches = data_shapes[0][0], num_batches

        self.image_aggregator = ImagesWithArtifactsAggregator(info_path, artifacts_parameters,
                                                              label_type, self.target_size)
        self.cur_batch = 0

    def __iter__(self):
        return self

    def reset(self):
        self.cur_batch = 0

    @property
    def provide_data(self):
        return self._provide_data

    @property
    def provide_label(self):
        return self._provide_label

    @property
    def num_samples(self):
        return self.num_batches

    def __next__(self):
        """Iterate over dataset. Collects batch and return it to network.

        :return:
        """
        self.cur_batch += 1
        if self.cur_batch < self.num_batches:
            data = []
            labels = []
            for _ in range(self.batch_size):
                data_sample, label = next(self.image_aggregator)
                data.append(data_sample)
                labels.append(label)
            return mx.io.DataBatch(data=[mx.nd.array(data)], label=[mx.nd.array(labels)])
        else:
            raise StopIteration
