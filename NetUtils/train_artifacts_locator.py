"""
Created by Andrey Belyaev
"""
import argparse
import logging
import mxnet as mx
import yaml
from easydict import EasyDict as edict

from DataUtils.DataIterators import ImagesWithArtifactsDataIter
from NetUtils.metrics import ArtifactsLocateMetric
from NetUtils.models import ArtifactsLocatorModel

logging.getLogger().setLevel(logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, default='config.yml')
    return parser.parse_args()


def train(config):
    nc, ac = config.nets_params, config.artifacts_params

    # Create train and test iterators
    train_iter = ImagesWithArtifactsDataIter(nc.train_info_path, ac, 'artifacts_map',
                                             [nc.data_name], [(nc.batch_size, 3) + tuple(nc.target_size)],
                                             [nc.label_name], [(nc.batch_size, 1) + tuple(nc.target_size)],
                                             num_batches=500)
    val_iter = ImagesWithArtifactsDataIter(nc.val_info_path, ac, 'artifacts_map',
                                           [nc.data_name], [(nc.batch_size, 3) + tuple(nc.target_size)],
                                           [nc.label_name], [(nc.batch_size, 1) + tuple(nc.target_size)])

    # Optimizer params
    lr_sch = mx.lr_scheduler.FactorScheduler(step=1000, factor=0.1, stop_factor_lr=0.000008)
    opt_params = {
        'learning_rate': 0.001,
        'lr_scheduler': lr_sch,
        'beta1': 0.9,
        'beta2': 0.99
    }

    # Create model
    art_loc_model = ArtifactsLocatorModel(nc.data_name, nc.label_name, nc.batch_size, nc.context).get_model()
    arg_params, aux_params = {}, {}

    # Fit model
    art_loc_model.fit(train_data=train_iter, eval_data=val_iter, eval_metric=ArtifactsLocateMetric(),
                      num_epoch=100, optimizer='adam', optimizer_params=opt_params,
                      batch_end_callback=[mx.callback.Speedometer(nc.batch_size, 10)],
                      epoch_end_callback=[mx.callback.do_checkpoint('nets/artifacts_locator_new_train')],
                      initializer=mx.initializer.Xavier('uniform'),
                      arg_params=arg_params, aux_params=aux_params, allow_missing=True
                      )


if __name__ == '__main__':
    args = parse_args()
    with open(args.config, 'r') as f:
        c = edict(yaml.load(f, Loader=yaml.FullLoader))
    train(c)
