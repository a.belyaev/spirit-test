"""
Created by Andrey Belyaev
"""
import argparse
import cv2
import mxnet as mx
import numpy as np
import yaml
from easydict import EasyDict as edict
from DataUtils.DataIterators import ImagesWithArtifactsDataIter
from tqdm import tqdm


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, default='config.yml')
    parser.add_argument('-n', '--net-path', type=str, default='nets/artifacts_locator')
    parser.add_argument('-e', '--epoch', type=int, default=0)
    parser.add_argument('-i', '--info-path', type=str, default='mpii_test.json')
    parser.add_argument('-t', '--label-type', type=str, default='artifacts_map')
    return parser.parse_args()


def visualize(config, net_path, net_epoch, info_path, label_type):
    bs = 1
    nc, ac = config.nets_params, config.artifacts_params
    sym, arg_params, aux_params = mx.model.load_checkpoint(net_path, net_epoch)
    model = mx.mod.Module(sym, data_names=['data'], label_names=['label'], context=mx.gpu(0))
    model.bind(data_shapes=[('data', (bs, 3) + tuple(nc.target_size))],
               label_shapes=[(nc.label_name, (bs, 1) + tuple(nc.target_size))], for_training=False)
    model.set_params(arg_params=arg_params, aux_params=aux_params)

    it = ImagesWithArtifactsDataIter(info_path, ac, label_type,
                                     [nc.data_name], [(1, 3) + tuple(nc.target_size)],
                                     [nc.label_name], [(1, 1) + tuple(nc.target_size)])
    for n, batch in tqdm(enumerate(it)):
        model.forward(batch, is_train=False)
        pred = model.get_outputs()[0].asnumpy()[0]
        pred[pred < 0.3] = 0.

        collage = np.hstack((batch.data[0].asnumpy()[0].transpose((1, 2, 0)),
                            np.repeat(batch.label[0].asnumpy()[0].transpose((1, 2, 0)), 3, -1),
                            np.repeat(pred.transpose((1, 2, 0)), 3, -1)))

        cv2.imshow('1', collage)
        if cv2.waitKey(0) & 0xff == ord('q'):
            break


if __name__ == '__main__':
    args = parse_args()
    with open(args.config, 'r') as f:
        c = edict(yaml.load(f, Loader=yaml.FullLoader))
    visualize(c, args.net_path, args.epoch, args.info_path, args.label_type)
