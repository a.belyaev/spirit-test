"""
Created by Andrey Belyaev
"""
import mxnet as mx
import numpy as np


class ArtifactsLocateMetric(mx.metric.EvalMetric):
    def __init__(self, name='artifacts_locate', output_names=None, label_names=None):
        super().__init__(name, output_names=output_names, label_names=label_names)

    def update(self, labels, preds):
        loss = np.sum(np.round(np.abs(labels[0].asnumpy() - preds[0].asnumpy()))) / float(labels[0].shape[0])
        loss /= float(labels[0].shape[2] * labels[0].shape[3])
        self.sum_metric += loss
        self.num_inst += 1
