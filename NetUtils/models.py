"""
Created by Andrey Belyaev
"""
import mxnet as mx


class ArtifactsLocatorModel(object):
    """Model for artifacts localization

    :param data_name: input node name
    :param label_name: label node name
    :param batch_size: batch size
    :param context: list of GPU numbers
    """
    def __init__(self, data_name, label_name, batch_size=16, context=(0,)):
        self.data_name, self.label_name = data_name, label_name
        self.data_var, self.label_var = mx.sym.Variable(data_name), mx.sym.Variable(label_name)
        self.batch_size, self.context = batch_size, list(map(lambda c: mx.gpu(int(c)), context))

    def get_model(self, compile_model=True):
        """Build a model

        :param compile_model: whether to build mxnet Module or not
        :return: model
        """
        cur_layer = self.data_var
        for i in range(8):
            cur_layer = mx.sym.Convolution(cur_layer, kernel=(5, 5), pad=(2, 2), num_filter=32, name=f'conv_{i}')
            cur_layer = mx.sym.Activation(cur_layer, act_type='relu', name=f'act_{i}')

        cur_layer = mx.sym.Convolution(cur_layer, kernel=(1, 1), num_filter=1, name='conv_last')

        # For first training use logistic regression
        out_layer = mx.sym.LogisticRegressionOutput(cur_layer, self.label_var)

        # # After successful logistic regression training, finetune your net by training with linear regression
        # cur_layer = mx.sym.Activation(cur_layer, act_type='sigmoid')
        # out_layer = mx.sym.LinearRegressionOutput(cur_layer, self.label_var)

        if compile_model:
            return mx.mod.Module(out_layer, label_names=[self.label_name], context=self.context)
        else:
            return out_layer
