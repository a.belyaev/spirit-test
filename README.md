# Spirit Test

### Requirements

1. Python 3.7
2. CUDA 10.0


### Installation

`python3.7 -m pip install -r requirements.txt`

Run in the project's root: `export PYTHONPATH=$PWD`


### Project structure

This project contains first part of solving the task:
artifacts localization.
There are scripts for prepare data, train network and test it.

#### Prepare dataset for training

Assume that you have your dataset in `/dataset` folder. Run
`python3.7 scripts/split_dataset.py --data-path /dataset
--out-path dataset_info`.

This will create two files:
`dataset_info_train.json` and `dataset_info_test.json` with paths to
images. Also, you can add `--parse-recursively` flag to parse all
subfolders in `/dataset`.

#### Prepare configs for training

Go to `scripts/generate_config.py` file. You may edit any parameters
in `generate_config` function. Change train and val path to your
generated `info_*.json` files.

After that, generate config by running
`python3.7 scripts/generate_config.py`. You can add flag
`--out-name=SOME_CONFIG_NAME` and generate many different configs for
training. By default config will be stored in `config.yml`

#### Train network

There are simple conv network in `NetUtils/models.py`, and simple
script for train it `NetUtils/train_artifact_locator.py`.
Just run

`mkdir -p nets; python3.7 NetUtils/train_artifacts_locator.py
-c config.yml`

This will train network and stores checkpoints into the `net/` folder.

Default configs are valid for training on GTX1080Ti.

#### Validate network

There is a simple script to visualize network predictions. 

If you want to see predictions on your test part of dataset, run

`python3.7 NetUtils/visualize_net_results.py -c config.yml
--nnet-path nets/artifacts_locator --epoch 0
--info-path dataset_info_test.json --label-type artifacts_map`

If you want to see predictions on Spirit dataset, then first prepare it:

`python3.7 scripts/prepare_spirit_validation.py --data-path
/YOUR/PATH/TO/SPIRIT/DATASET--out-path spirit_data`

and then run visualization

`python3.7 NetUtils/visualize_net_results.py -c config.yml
--nnet-path nets/artifacts_locator --epoch 0
--info-path spirit_data_info.json --label-type from_info`


### Examples

You can see examples in `examples/` folder.